# About #

GNU Emacs configuration I use, not likely to be useful as-is, but some code snippets or configuration tricks may come in handy.

## Compatibility ##

I use fresh [Emacs from git](https://git.savannah.gnu.org/git/emacs.git/), this configuration is usually updated as needed to be compatible with it.
