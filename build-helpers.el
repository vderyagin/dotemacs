(defun vd/batch-tangle ()
  (require 'ob-tangle)

  (add-hook 'org-babel-post-tangle-hook
            (lambda ()
              (goto-char (point-min))
              (insert ";;; -*- lexical-binding: t -*-\n")
              (save-buffer 0)))

  (mapc #'org-babel-tangle-file command-line-args-left))

(defun vd/upgrade-packages ()
  (straight-pull-recipe-repositories)
  (straight-pull-all)
  (straight-check-all))

(defun vd/native-compile-sync-with-progress (&rest args)
  (if (native-comp-available-p)
      (let* ((_ (apply #'native-compile-async args))
             (queue-length (length comp-files-queue))
             (progress (make-progress-reporter "Compiling" 0 queue-length)))
        (while comp-files-queue
          (progress-reporter-update progress (- queue-length (length comp-files-queue)))
          (sleep-for 1))
        (progress-reporter-done progress))
    (message "Native compilation is not available")))

(defun vd/batch-native-compile ()
  (vd/native-compile-sync-with-progress
   command-line-args-left
   'recursively
   nil
   (lambda (file)
     (and
      (string-suffix-p ".el" file)
      (not (string-suffix-p ".dir-locals.el" file))
      (not (string-suffix-p "-autoloads.el" file))
      (not (string-suffix-p "-pkg.el" file))))))
