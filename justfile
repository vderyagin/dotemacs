# build all the stuff necessary
default: build

emacs-major-version := `emacs --quick --batch --eval '(princ emacs-major-version)'`

# build all the stuff necessary
build: color-theme init sqlite

# compile emacsql-sqlite binary
sqlite:
    emacs \
      --quick --batch \
      --load ~/.emacs.d/init \
      --load emacsql-sqlite \
      --funcall emacsql-sqlite-ensure-binary

# recompile init & dependencies from scratch
rebuild: _clean-all build

# build Emacs init file
init: _tangle _byte-compile-init _native-compile _install-init _clean-build-artifacts

# upgrade dependencies
upgrade: _tangle _upgrade _byte-compile-init _native-compile _install-init _clean-build-artifacts
    @just build

# remove unused dependencies
cleanup-dependencies: _tangle && build
    emacs \
      --quick --batch \
      --load init.el \
      --eval "(straight-remove-unused-repos 'force)"

# build & install color theme
color-theme: _tangle
    mkdir --parents ~/.emacs.d/color-theme-quiet-light

    emacs \
      --quick --batch \
      --funcall batch-byte-compile quiet-light-theme.el

    cp \
      --force \
        quiet-light-theme.el{,c} \
        ~/.emacs.d/color-theme-quiet-light/

    rm quiet-light-theme.elc

# list Emacs Lisp source files that got no corresponding bytecode
uncompiled:
    #! /usr/bin/env ruby
    # frozen_string-literal: true

    Dir.glob(File.expand_path('~/.emacs.d/straight/build-*/**/*.el'))
      .grep_v(%r{-(pkg|autoloads|loaddefs)\.el\z})
      .reject { |src| File.exist?("#{src}c") }
      .reject { |src| File.read(src)[%r{no-byte-compile: t}] }
      .each { |src| puts src }

# upgrade Emacs
upgrade-emacs:
    @if [ `uname` = Linux ]; then \
      sudo emerge --jobs=1 emacs:{{emacs-major-version}}-vcs; \
    elif [ `uname` = Darwin ]; then \
      brew upgrade --cask --greedy --verbose emacs-nightly; \
    else \
      just _unsupported_os; \
    fi

# check if all necessary dependencies are installed
deps:
    @if [ `uname` = Linux ]; then \
      just _check_installed_dependencies_linux; \
    elif [ `uname` = Darwin ]; then \
      just _check_installed_dependencies_macos; \
    else \
      just _unsupported_os; \
    fi

noweb-lint:
    #! /usr/bin/env ruby
    # frozen_string-literal: true

    require 'set'

    init = File.read('./init.org')
    expansions = Set.new(init.scan(/<<([^>]+)>>/).map(&:first))
    block_names = Set.new(init.scan(/^#\+name:\s*(.+)$/i).map(&:first))

    unless expansions == block_names
      warn 'noweb template inconsistency'
      exit 1
    end

_check_installed_dependencies_linux:
    #! /usr/bin/env ruby
    # frozen_string-literal: true

    dependencies = {
      binaries: %w[gnutls-cli hsmarkdown ssh go goreturns rg zsh msmtp rake gpg git vale mpv mupdf foliate feh vlc mcomix],
      packages: %w[
        media-gfx/ditaa-bin
        dev-util/global
        app-office/ledger
        www-client/brave-bin
        media-fonts/fira-code
        media-fonts/dejavu
      ],
    }

    exit_code = 0

    installed_packages = `EIX_LIMIT=0 eix --installed --only-names`.lines.map(&:chomp)

    lacking_binaries = dependencies[:binaries].reject { |binary|
      system "which #{binary} >/dev/null 2>&1"
    }

    unless lacking_binaries.empty?
      warn "Some binaries are missing: #{lacking_binaries.join(', ')}"
      exit_code = 1
    end

    lacking_packages = dependencies[:packages] - installed_packages

    unless lacking_packages.empty?
      warn "Some package dependencies are not installed: #{lacking_packages.join(', ')}"
      exit_code = 1
    end

    exit exit_code

_check_installed_dependencies_macos:
    #! /usr/bin/env ruby
    # frozen_string-literal: true

    dependencies = {
      binaries: %w[gnutls-cli hsmarkdown ssh go goreturns rg zsh msmtp rake gpg git vale],
      packages: %w[ditaa coreutils global ledger],
      casks: %w[brave-browser font-fira-code font-dejavu mpv],
    }

    installed_packages = `brew leaves`.lines.map(&:chomp)
    installed_casks = `brew list --casks -1`.lines.map(&:chomp)

    exit_code = 0

    lacking_binaries = dependencies[:binaries].reject { |binary|
      system "which #{binary} >/dev/null 2>&1"
    }

    unless lacking_binaries.empty?
      warn "Some binaries are missing: #{lacking_binaries.join(', ')}"
      exit_code = 1
    end

    lacking_packages = dependencies[:packages] - installed_packages

    unless lacking_packages.empty?
      warn "Some package dependencies are not installed: #{lacking_packages.join(', ')}"
      exit_code = 1
    end

    lacking_casks = dependencies[:casks] - installed_casks

    unless lacking_casks.empty?
      warn "Some cask dependencies are not installed: #{lacking_casks.join(', ')}"
      exit_code = 1
    end

    exit exit_code

_clean-all: _clean-build-artifacts
    rm \
      --recursive --force \
        ~/.emacs.d/straight/build-* \
        ~/.emacs.d/eln-cache \
        ~/.emacs.d/color-theme-quiet-light

_tangle: noweb-lint
    emacs \
      --quick --batch \
      --load build-helpers.el \
      --funcall vd/batch-tangle ./init.org

_native-compile:
    emacs \
      --quick --batch \
      --load init.el \
      --load build-helpers.el \
      --funcall vd/batch-native-compile \
        ~/.emacs.d/straight/build-{{emacs-major-version}}/ \
        ./init.el \
        ./bmk.el.gpg \
        ./abbrev_defs.el

_byte-compile-init:
    emacs \
      --quick --batch \
      --load init.el \
      --funcall batch-byte-compile \
        {,early-}init.el \
        abbrev_defs.el \
        bmk.el.gpg

_install-init:
    cp \
      early-init.el \
      init.el{,c} \
      ~/.emacs.d/

    gzip -c ./bmk.el.gpg.elc > ~/.emacs.d/bmk.elc.gz
    cp ./abbrev_defs.elc ~/.emacs.d/abbrev_defs

_clean-build-artifacts:
    rm --force \
       {,early-}init.el \
       quiet-light-theme.el \
       *.elc

_upgrade:
    emacs \
      --quick --batch \
      --load init.el \
      --load build-helpers.el \
      --funcall vd/upgrade-packages

@_unsupported-os:
    echo "Unsupported OS: {{os()}}"
    exit 1
